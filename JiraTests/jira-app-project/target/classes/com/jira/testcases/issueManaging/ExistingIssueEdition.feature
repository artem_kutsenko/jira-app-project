@edit
Feature: Existing issue edition

As a Chuck Norris
In order to manage content of the existing issues
I want to be able to edit them


Scenario Outline: Editing existing bug

Given RunMode is "<RunMode>"
	And I am on the home page in "<browserType>"
	And I open one of the issues
	And I add "<newBugDescription>" to the issue description 
When I update the issue
Then My changes should be applied
	And I sign out
	
Examples:
| RunMode |	browserType	| newBugDescription                                          |
| Y       |	Mozilla   	| You are playing with the fire, kid. Sincerely yours, Chuck |
| N       |	Chrome   	| You are playing with the fire, kid. Sincerely yours, Chuck |
| N       |	IE      	| You are playing with the fire, kid. Sincerely yours, Chuck |		