package com.jira.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/*defining the list of private elements on the page NewIssuePage. 
 *the getters return WebElement 
 */

public class NewIssuePage {
	
	private static NewIssuePage newIssuePage;
	private final WebDriver driver;
	
	private NewIssuePage(WebDriver dr){	
		driver=dr;
	}
	
	private WebElement issueTypeDropDown;
	private WebElement assignOnMe;
	private WebElement summaryField;
	private WebElement priorityDropDown;
	private WebElement componentsDropDown;
	private WebElement fixVersionsDropDown;
	private WebElement descriptionField;
	private WebElement createIssueButton;
	
	
	public WebElement getIssueTypeDropDown(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		issueTypeDropDown = wait.until(ExpectedConditions.elementToBeClickable(By.id("issuetype-field")));
		return issueTypeDropDown;
	}
	
	public WebElement getAssignOnMe(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		assignOnMe = wait.until(ExpectedConditions.elementToBeClickable(By.id("assign-to-me-trigger")));
		return assignOnMe;
	}
	
	public WebElement getSummaryField(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		summaryField = wait.until(ExpectedConditions.elementToBeClickable(By.id("summary")));
		return summaryField;
	}
	
	public WebElement getPriorityDropDown(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		priorityDropDown = wait.until(ExpectedConditions.elementToBeClickable(By.id("priority-field")));
		return priorityDropDown;
	}
	
	public WebElement getComponentsDropDown(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		componentsDropDown = wait.until(ExpectedConditions.elementToBeClickable(By.id("components-textarea")));
		return componentsDropDown;
	}
	
	public WebElement getFixVersionsDropDown(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		fixVersionsDropDown = wait.until(ExpectedConditions.elementToBeClickable(By.id("fixVersions-textarea")));
		return fixVersionsDropDown;
	}
	
	public WebElement getDescriptionField(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		descriptionField = wait.until(ExpectedConditions.elementToBeClickable(By.id("description")));
		return descriptionField;
	}
	
	public WebElement getCreateIssueButton(){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		createIssueButton = wait.until(ExpectedConditions.elementToBeClickable(By.id("create-issue-submit")));
		return createIssueButton;
	}

		
	//Using singletone to get the instance of NewIssuePage
	public static synchronized NewIssuePage getInstance (WebDriver dr){
		if (newIssuePage==null){
			newIssuePage = new NewIssuePage(dr);
		}
		return newIssuePage;
	}	

}
