@create
Feature: New bug creation

As a Jira user
In order to report existing issue
I want to create any bug


Scenario Outline: Creating new bug

Given RunMode is "<RunMode>"
	And I am on the home page in "<browserType>"
When I start the creation of new issue
	And I choose "Bug" issuetype	
	And I specify the Summary as "<bugSummary>"
	And I choose "<bugPriority>" priority
	And I choose the "Component" which contains the issue 
	And I specify "1.0.0" Fix version
	And I assign issue on me
	And I specify the issue description as "<bugDescription>"
	And I create the issue
Then I should see the successful message "<message>"
	And Issue should be created with Summary "<bugSummary>"
	And I sign out

Examples:
| RunMode |	browserType	| bugSummary                           | bugPriority | bugDescription                    | message                        |
| Y       |	Mozilla   	| Chuck Norris is not strong enough    | High        | My Grandmom is able to kick him   | has been successfully created. |
| N       |	Chrome   	| Jackie Chan is stronger than Chuck   | Highest     | Jackie Chan is able to kick him   | has been successfully created. |
| N       |	IE      	| Steven Seagal is stronger than Chuck | Highest     | Steven Seagal is able to kick him | has been successfully created. |