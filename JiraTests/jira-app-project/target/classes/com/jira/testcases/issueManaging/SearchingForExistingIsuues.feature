@search
Feature: Searching for existing issues

As a Jira user
In order to check the list of existing issues
I want to be able to find them using Jira's search


Scenario Outline: Searching for existing issue

Given RunMode is "<RunMode>"
	And I am on the home page in "<browserType>"
When I perform a quick search for issue which contains "<text>"
Then At least one corresponding issue has to be found
	And I check that description of the top issue contains "<text>"
	And I sign out
	
Examples:
| RunMode |	browserType	| text     |
| Y       |	Mozilla   	| GrandMom |
| N       |	Chrome   	| Jackie   |
| N       |	IE      	| GrandMom |