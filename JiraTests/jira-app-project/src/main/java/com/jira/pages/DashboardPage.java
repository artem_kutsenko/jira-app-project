package com.jira.pages;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/*defining the list of private elements on the page DashboardPage. 
 *the getters return WebElement 
 */

public class DashboardPage {
	
	private static DashboardPage dsbPage;
	private final WebDriver driver;

	
	private DashboardPage(WebDriver dr){
		driver = dr;
	}
		public WebElement createNewIssueButton;
		public WebElement projectsMenu;
		public WebElement homeProject;
		public WebElement issuesMenu;
		public WebElement searchForIssuesOption;
		public WebElement headerDetailsMenu;
		public WebElement logOut;
		public WebElement notificationWindow;
		public WebElement newlyCreatedIssueLink;
		public WebElement issuesReportedByMe;
		public WebElement issuesNewSearchLink;
		public WebElement quickSearchField;
		
		
		public WebElement getQuickSearchField(){
			WebDriverWait wait = new WebDriverWait(driver, 15);
			quickSearchField = wait.until(ExpectedConditions.elementToBeClickable(By.id("quickSearchInput")));
			return quickSearchField;
		}
		
		public WebElement getIssuesNewSearchLink(){
			WebDriverWait wait = new WebDriverWait(driver, 15);
			issuesReportedByMe = wait.until(ExpectedConditions.elementToBeClickable(By.id("issues_new_search_link_lnk")));
			return issuesReportedByMe;
		}
		
		public WebElement getIssuesReportedByMe(){
			WebDriverWait wait = new WebDriverWait(driver, 15);
			issuesReportedByMe = wait.until(ExpectedConditions.elementToBeClickable(By.id("filter_lnk_reported_lnk")));
			return issuesReportedByMe;
		}
		
		public WebElement getIssuesMenu(){
			WebDriverWait wait = new WebDriverWait(driver, 15);
			issuesMenu = wait.until(ExpectedConditions.elementToBeClickable(By.id("find_link")));
			return issuesMenu;
		}
		
		public WebElement getLogOut(){
			WebDriverWait wait = new WebDriverWait(driver, 15);
			logOut = wait.until(ExpectedConditions.elementToBeClickable(By.id("log_out")));
			return logOut;
		}
		
		public WebElement getNotificationWindow(){
			WebDriverWait wait = new WebDriverWait(driver, 15);
			notificationWindow = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".aui-message.aui-message-success.success.closeable.shadowed")));
			return notificationWindow;
		}
		
		public String clickIssueLinkFromNotificationWindow(){
			try{
				WebDriverWait wait = new WebDriverWait(driver, 15);
			
				String parentWindowHandler = driver.getWindowHandle();
				String popupWindowHandler = null;

				Set<String> handles = driver.getWindowHandles();
				Iterator<String> iterator = handles.iterator();
			
				while (iterator.hasNext()){
					popupWindowHandler = iterator.next();
				}		
				driver.switchTo().window(popupWindowHandler); 
				newlyCreatedIssueLink= wait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText("HOM-")));
				newlyCreatedIssueLink.click();
				driver.switchTo().window(parentWindowHandler);	
				return "PASS";
			}catch(Exception e){
				System.out.println();	
			return "FAIL";
				
		}
		}
		
		public WebElement getHeaderDetailsMenu(){
			WebDriverWait wait = new WebDriverWait(driver, 15);
			headerDetailsMenu = wait.until(ExpectedConditions.elementToBeClickable(By.id("header-details-user-fullname")));
			return headerDetailsMenu;
		}
		
		public WebElement getCreateNewIssueButton(){
			WebDriverWait wait = new WebDriverWait(driver, 15);
			createNewIssueButton = wait.until(ExpectedConditions.elementToBeClickable(By.id("create_link")));
			return createNewIssueButton;
		}
		
		
		//Using singletone to get the instance of DashboardPage
		public static synchronized DashboardPage getInstance (WebDriver dr){
			if (dsbPage==null){
				dsbPage = new DashboardPage(dr);
			}
			return dsbPage;
		}
}
