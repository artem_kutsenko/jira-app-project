$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("main/resources/com/jira/testcases/issueManaging/BugCreation.feature");
formatter.feature({
  "id": "new-bug-creation",
  "tags": [
    {
      "name": "@create",
      "line": 1
    }
  ],
  "description": "\nAs a Jira user\nIn order to report existing issue\nI want to create any bug",
  "name": "New bug creation",
  "keyword": "Feature",
  "line": 2
});
formatter.scenarioOutline({
  "id": "new-bug-creation;creating-new-bug",
  "description": "",
  "name": "Creating new bug",
  "keyword": "Scenario Outline",
  "line": 9,
  "type": "scenario_outline"
});
formatter.step({
  "name": "RunMode is \"\u003cRunMode\u003e\"",
  "keyword": "Given ",
  "line": 11
});
formatter.step({
  "name": "I am on the home page in \"\u003cbrowserType\u003e\"",
  "keyword": "And ",
  "line": 12
});
formatter.step({
  "name": "I start the creation of new issue",
  "keyword": "When ",
  "line": 13
});
formatter.step({
  "name": "I choose \"Bug\" issuetype",
  "keyword": "And ",
  "line": 14
});
formatter.step({
  "name": "I specify the Summary as \"\u003cbugSummary\u003e\"",
  "keyword": "And ",
  "line": 15
});
formatter.step({
  "name": "I choose \"\u003cbugPriority\u003e\" priority",
  "keyword": "And ",
  "line": 16
});
formatter.step({
  "name": "I choose the \"Component\" which contains the issue",
  "keyword": "And ",
  "line": 17
});
formatter.step({
  "name": "I specify \"1.0.0\" Fix version",
  "keyword": "And ",
  "line": 18
});
formatter.step({
  "name": "I assign issue on me",
  "keyword": "And ",
  "line": 19
});
formatter.step({
  "name": "I specify the issue description as \"\u003cbugDescription\u003e\"",
  "keyword": "And ",
  "line": 20
});
formatter.step({
  "name": "I create the issue",
  "keyword": "And ",
  "line": 21
});
formatter.step({
  "name": "I should see the successful message \"\u003cmessage\u003e\"",
  "keyword": "Then ",
  "line": 22
});
formatter.step({
  "name": "Issue should be created with Summary \"\u003cbugSummary\u003e\"",
  "keyword": "And ",
  "line": 23
});
formatter.step({
  "name": "I sign out",
  "keyword": "And ",
  "line": 24
});
formatter.examples({
  "id": "new-bug-creation;creating-new-bug;",
  "description": "",
  "name": "",
  "keyword": "Examples",
  "line": 26,
  "rows": [
    {
      "id": "new-bug-creation;creating-new-bug;;1",
      "cells": [
        "RunMode",
        "browserType",
        "bugSummary",
        "bugPriority",
        "bugDescription",
        "message"
      ],
      "line": 27
    },
    {
      "id": "new-bug-creation;creating-new-bug;;2",
      "cells": [
        "N",
        "Mozilla",
        "Chuck Norris is not strong enough",
        "High",
        "My Grandmom is able to kick him",
        "has been successfully created."
      ],
      "line": 28
    },
    {
      "id": "new-bug-creation;creating-new-bug;;3",
      "cells": [
        "Y",
        "Chrome",
        "Jackie Chan is stronger than Chuck",
        "Highest",
        "Jackie Chan is able to kick him",
        "has been successfully created."
      ],
      "line": 29
    },
    {
      "id": "new-bug-creation;creating-new-bug;;4",
      "cells": [
        "N",
        "IE",
        "Steven Seagal is stronger than Chuck",
        "Highest",
        "Steven Seagal is able to kick him",
        "has been successfully created."
      ],
      "line": 30
    }
  ]
});
formatter.scenario({
  "id": "new-bug-creation;creating-new-bug;;2",
  "tags": [
    {
      "name": "@create",
      "line": 1
    }
  ],
  "description": "",
  "name": "Creating new bug",
  "keyword": "Scenario Outline",
  "line": 28,
  "type": "scenario"
});
formatter.step({
  "name": "RunMode is \"N\"",
  "keyword": "Given ",
  "line": 11,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "I am on the home page in \"Mozilla\"",
  "keyword": "And ",
  "line": 12,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "I start the creation of new issue",
  "keyword": "When ",
  "line": 13
});
formatter.step({
  "name": "I choose \"Bug\" issuetype",
  "keyword": "And ",
  "line": 14
});
formatter.step({
  "name": "I specify the Summary as \"Chuck Norris is not strong enough\"",
  "keyword": "And ",
  "line": 15,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "I choose \"High\" priority",
  "keyword": "And ",
  "line": 16,
  "matchedColumns": [
    3
  ]
});
formatter.step({
  "name": "I choose the \"Component\" which contains the issue",
  "keyword": "And ",
  "line": 17
});
formatter.step({
  "name": "I specify \"1.0.0\" Fix version",
  "keyword": "And ",
  "line": 18
});
formatter.step({
  "name": "I assign issue on me",
  "keyword": "And ",
  "line": 19
});
formatter.step({
  "name": "I specify the issue description as \"My Grandmom is able to kick him\"",
  "keyword": "And ",
  "line": 20,
  "matchedColumns": [
    4
  ]
});
formatter.step({
  "name": "I create the issue",
  "keyword": "And ",
  "line": 21
});
formatter.step({
  "name": "I should see the successful message \"has been successfully created.\"",
  "keyword": "Then ",
  "line": 22,
  "matchedColumns": [
    5
  ]
});
formatter.step({
  "name": "Issue should be created with Summary \"Chuck Norris is not strong enough\"",
  "keyword": "And ",
  "line": 23,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "I sign out",
  "keyword": "And ",
  "line": 24
});
formatter.match({
  "arguments": [
    {
      "val": "N",
      "offset": 12
    }
  ],
  "location": "CommonUtil.checkRunMode(String)"
});
formatter.result({
  "duration": 150602000,
  "status": "pending",
  "error_message": "cucumber.api.PendingException: Skipping the test dataset cause runmode \u003d N\n\tat com.jira.testcases.CommonUtil.checkRunMode(CommonUtil.java:24)\n\tat ✽.Given RunMode is \"N\"(main/resources/com/jira/testcases/issueManaging/BugCreation.feature:11)\n"
});
formatter.match({
  "arguments": [
    {
      "val": "Mozilla",
      "offset": 26
    }
  ],
  "location": "CommonUtil.loginWithDefaultCredentials(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BugCreation.startNewFeatureCreation()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Bug",
      "offset": 10
    }
  ],
  "location": "BugCreation.selectIssueType(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Chuck Norris is not strong enough",
      "offset": 26
    }
  ],
  "location": "BugCreation.specifyIssueSummary(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "High",
      "offset": 10
    }
  ],
  "location": "BugCreation.specifyIssuePriority(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Component",
      "offset": 14
    }
  ],
  "location": "BugCreation.chooseIssueComponent(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "1.0.0",
      "offset": 11
    }
  ],
  "location": "BugCreation.chooseFixVersion(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BugCreation.assignIssueOnMe()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "My Grandmom is able to kick him",
      "offset": 36
    }
  ],
  "location": "BugCreation.fillIssueDescription(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BugCreation.finishIssueCreation()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "has been successfully created.",
      "offset": 37
    }
  ],
  "location": "BugCreation.CheckIssueCreationMessage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Chuck Norris is not strong enough",
      "offset": 38
    }
  ],
  "location": "BugCreation.CheckIssueCreated(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CommonUtil.signOut()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "id": "new-bug-creation;creating-new-bug;;3",
  "tags": [
    {
      "name": "@create",
      "line": 1
    }
  ],
  "description": "",
  "name": "Creating new bug",
  "keyword": "Scenario Outline",
  "line": 29,
  "type": "scenario"
});
formatter.step({
  "name": "RunMode is \"Y\"",
  "keyword": "Given ",
  "line": 11,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "I am on the home page in \"Chrome\"",
  "keyword": "And ",
  "line": 12,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "I start the creation of new issue",
  "keyword": "When ",
  "line": 13
});
formatter.step({
  "name": "I choose \"Bug\" issuetype",
  "keyword": "And ",
  "line": 14
});
formatter.step({
  "name": "I specify the Summary as \"Jackie Chan is stronger than Chuck\"",
  "keyword": "And ",
  "line": 15,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "I choose \"Highest\" priority",
  "keyword": "And ",
  "line": 16,
  "matchedColumns": [
    3
  ]
});
formatter.step({
  "name": "I choose the \"Component\" which contains the issue",
  "keyword": "And ",
  "line": 17
});
formatter.step({
  "name": "I specify \"1.0.0\" Fix version",
  "keyword": "And ",
  "line": 18
});
formatter.step({
  "name": "I assign issue on me",
  "keyword": "And ",
  "line": 19
});
formatter.step({
  "name": "I specify the issue description as \"Jackie Chan is able to kick him\"",
  "keyword": "And ",
  "line": 20,
  "matchedColumns": [
    4
  ]
});
formatter.step({
  "name": "I create the issue",
  "keyword": "And ",
  "line": 21
});
formatter.step({
  "name": "I should see the successful message \"has been successfully created.\"",
  "keyword": "Then ",
  "line": 22,
  "matchedColumns": [
    5
  ]
});
formatter.step({
  "name": "Issue should be created with Summary \"Jackie Chan is stronger than Chuck\"",
  "keyword": "And ",
  "line": 23,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "I sign out",
  "keyword": "And ",
  "line": 24
});
formatter.match({
  "arguments": [
    {
      "val": "Y",
      "offset": 12
    }
  ],
  "location": "CommonUtil.checkRunMode(String)"
});
formatter.result({
  "duration": 103000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrome",
      "offset": 26
    }
  ],
  "location": "CommonUtil.loginWithDefaultCredentials(String)"
});
formatter.result({
  "duration": 15929603000,
  "status": "passed"
});
formatter.match({
  "location": "BugCreation.startNewFeatureCreation()"
});
formatter.result({
  "duration": 159037000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Bug",
      "offset": 10
    }
  ],
  "location": "BugCreation.selectIssueType(String)"
});
formatter.result({
  "duration": 1256052000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jackie Chan is stronger than Chuck",
      "offset": 26
    }
  ],
  "location": "BugCreation.specifyIssueSummary(String)"
});
formatter.result({
  "duration": 1744499000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Highest",
      "offset": 10
    }
  ],
  "location": "BugCreation.specifyIssuePriority(String)"
});
formatter.result({
  "duration": 390466000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Component",
      "offset": 14
    }
  ],
  "location": "BugCreation.chooseIssueComponent(String)"
});
formatter.result({
  "duration": 437611000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1.0.0",
      "offset": 11
    }
  ],
  "location": "BugCreation.chooseFixVersion(String)"
});
formatter.result({
  "duration": 290689000,
  "status": "passed"
});
formatter.match({
  "location": "BugCreation.assignIssueOnMe()"
});
formatter.result({
  "duration": 153850000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jackie Chan is able to kick him",
      "offset": 36
    }
  ],
  "location": "BugCreation.fillIssueDescription(String)"
});
formatter.result({
  "duration": 712990000,
  "status": "passed"
});
formatter.match({
  "location": "BugCreation.finishIssueCreation()"
});
formatter.result({
  "duration": 191464000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "has been successfully created.",
      "offset": 37
    }
  ],
  "location": "BugCreation.CheckIssueCreationMessage(String)"
});
formatter.result({
  "duration": 1329886000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jackie Chan is stronger than Chuck",
      "offset": 38
    }
  ],
  "location": "BugCreation.CheckIssueCreated(String)"
});
formatter.result({
  "duration": 3743321000,
  "status": "passed"
});
formatter.match({
  "location": "CommonUtil.signOut()"
});
formatter.result({
  "duration": 1372823000,
  "status": "passed"
});
formatter.scenario({
  "id": "new-bug-creation;creating-new-bug;;4",
  "tags": [
    {
      "name": "@create",
      "line": 1
    }
  ],
  "description": "",
  "name": "Creating new bug",
  "keyword": "Scenario Outline",
  "line": 30,
  "type": "scenario"
});
formatter.step({
  "name": "RunMode is \"N\"",
  "keyword": "Given ",
  "line": 11,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "I am on the home page in \"IE\"",
  "keyword": "And ",
  "line": 12,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "I start the creation of new issue",
  "keyword": "When ",
  "line": 13
});
formatter.step({
  "name": "I choose \"Bug\" issuetype",
  "keyword": "And ",
  "line": 14
});
formatter.step({
  "name": "I specify the Summary as \"Steven Seagal is stronger than Chuck\"",
  "keyword": "And ",
  "line": 15,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "I choose \"Highest\" priority",
  "keyword": "And ",
  "line": 16,
  "matchedColumns": [
    3
  ]
});
formatter.step({
  "name": "I choose the \"Component\" which contains the issue",
  "keyword": "And ",
  "line": 17
});
formatter.step({
  "name": "I specify \"1.0.0\" Fix version",
  "keyword": "And ",
  "line": 18
});
formatter.step({
  "name": "I assign issue on me",
  "keyword": "And ",
  "line": 19
});
formatter.step({
  "name": "I specify the issue description as \"Steven Seagal is able to kick him\"",
  "keyword": "And ",
  "line": 20,
  "matchedColumns": [
    4
  ]
});
formatter.step({
  "name": "I create the issue",
  "keyword": "And ",
  "line": 21
});
formatter.step({
  "name": "I should see the successful message \"has been successfully created.\"",
  "keyword": "Then ",
  "line": 22,
  "matchedColumns": [
    5
  ]
});
formatter.step({
  "name": "Issue should be created with Summary \"Steven Seagal is stronger than Chuck\"",
  "keyword": "And ",
  "line": 23,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "I sign out",
  "keyword": "And ",
  "line": 24
});
formatter.match({
  "arguments": [
    {
      "val": "N",
      "offset": 12
    }
  ],
  "location": "CommonUtil.checkRunMode(String)"
});
formatter.result({
  "duration": 424000,
  "status": "pending",
  "error_message": "cucumber.api.PendingException: Skipping the test dataset cause runmode \u003d N\n\tat com.jira.testcases.CommonUtil.checkRunMode(CommonUtil.java:24)\n\tat ✽.Given RunMode is \"N\"(main/resources/com/jira/testcases/issueManaging/BugCreation.feature:11)\n"
});
formatter.match({
  "arguments": [
    {
      "val": "IE",
      "offset": 26
    }
  ],
  "location": "CommonUtil.loginWithDefaultCredentials(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BugCreation.startNewFeatureCreation()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Bug",
      "offset": 10
    }
  ],
  "location": "BugCreation.selectIssueType(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Steven Seagal is stronger than Chuck",
      "offset": 26
    }
  ],
  "location": "BugCreation.specifyIssueSummary(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Highest",
      "offset": 10
    }
  ],
  "location": "BugCreation.specifyIssuePriority(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Component",
      "offset": 14
    }
  ],
  "location": "BugCreation.chooseIssueComponent(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "1.0.0",
      "offset": 11
    }
  ],
  "location": "BugCreation.chooseFixVersion(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BugCreation.assignIssueOnMe()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Steven Seagal is able to kick him",
      "offset": 36
    }
  ],
  "location": "BugCreation.fillIssueDescription(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BugCreation.finishIssueCreation()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "has been successfully created.",
      "offset": 37
    }
  ],
  "location": "BugCreation.CheckIssueCreationMessage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Steven Seagal is stronger than Chuck",
      "offset": 38
    }
  ],
  "location": "BugCreation.CheckIssueCreated(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CommonUtil.signOut()"
});
formatter.result({
  "status": "skipped"
});
formatter.uri("main/resources/com/jira/testcases/issueManaging/ExistingIssueEdition.feature");
formatter.feature({
  "id": "existing-issue-edition",
  "tags": [
    {
      "name": "@edit",
      "line": 1
    }
  ],
  "description": "\nAs a Chuck Norris\nIn order to manage content of the existing issues\nI want to be able to edit them",
  "name": "Existing issue edition",
  "keyword": "Feature",
  "line": 2
});
formatter.scenarioOutline({
  "id": "existing-issue-edition;editing-existing-bug",
  "description": "",
  "name": "Editing existing bug",
  "keyword": "Scenario Outline",
  "line": 9,
  "type": "scenario_outline"
});
formatter.step({
  "name": "RunMode is \"\u003cRunMode\u003e\"",
  "keyword": "Given ",
  "line": 11
});
formatter.step({
  "name": "I am on the home page in \"\u003cbrowserType\u003e\"",
  "keyword": "And ",
  "line": 12
});
formatter.step({
  "name": "I open one of the issues",
  "keyword": "And ",
  "line": 13
});
formatter.step({
  "name": "I add \"\u003cnewBugDescription\u003e\" to the issue description",
  "keyword": "And ",
  "line": 14
});
formatter.step({
  "name": "I update the issue",
  "keyword": "When ",
  "line": 15
});
formatter.step({
  "name": "My changes should be applied",
  "keyword": "Then ",
  "line": 16
});
formatter.step({
  "name": "I sign out",
  "keyword": "And ",
  "line": 17
});
formatter.examples({
  "id": "existing-issue-edition;editing-existing-bug;",
  "description": "",
  "name": "",
  "keyword": "Examples",
  "line": 19,
  "rows": [
    {
      "id": "existing-issue-edition;editing-existing-bug;;1",
      "cells": [
        "RunMode",
        "browserType",
        "newBugDescription"
      ],
      "line": 20
    },
    {
      "id": "existing-issue-edition;editing-existing-bug;;2",
      "cells": [
        "N",
        "Mozilla",
        "You are playing with the fire, kid. Sincerely yours, Chuck"
      ],
      "line": 21
    },
    {
      "id": "existing-issue-edition;editing-existing-bug;;3",
      "cells": [
        "Y",
        "Chrome",
        "You are playing with the fire, kid. Sincerely yours, Chuck"
      ],
      "line": 22
    },
    {
      "id": "existing-issue-edition;editing-existing-bug;;4",
      "cells": [
        "N",
        "IE",
        "You are playing with the fire, kid. Sincerely yours, Chuck"
      ],
      "line": 23
    }
  ]
});
formatter.scenario({
  "id": "existing-issue-edition;editing-existing-bug;;2",
  "tags": [
    {
      "name": "@edit",
      "line": 1
    }
  ],
  "description": "",
  "name": "Editing existing bug",
  "keyword": "Scenario Outline",
  "line": 21,
  "type": "scenario"
});
formatter.step({
  "name": "RunMode is \"N\"",
  "keyword": "Given ",
  "line": 11,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "I am on the home page in \"Mozilla\"",
  "keyword": "And ",
  "line": 12,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "I open one of the issues",
  "keyword": "And ",
  "line": 13
});
formatter.step({
  "name": "I add \"You are playing with the fire, kid. Sincerely yours, Chuck\" to the issue description",
  "keyword": "And ",
  "line": 14,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "I update the issue",
  "keyword": "When ",
  "line": 15
});
formatter.step({
  "name": "My changes should be applied",
  "keyword": "Then ",
  "line": 16
});
formatter.step({
  "name": "I sign out",
  "keyword": "And ",
  "line": 17
});
formatter.match({
  "arguments": [
    {
      "val": "N",
      "offset": 12
    }
  ],
  "location": "CommonUtil.checkRunMode(String)"
});
formatter.result({
  "duration": 188000,
  "status": "pending",
  "error_message": "cucumber.api.PendingException: Skipping the test dataset cause runmode \u003d N\n\tat com.jira.testcases.CommonUtil.checkRunMode(CommonUtil.java:24)\n\tat ✽.Given RunMode is \"N\"(main/resources/com/jira/testcases/issueManaging/ExistingIssueEdition.feature:11)\n"
});
formatter.match({
  "arguments": [
    {
      "val": "Mozilla",
      "offset": 26
    }
  ],
  "location": "CommonUtil.loginWithDefaultCredentials(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "ExistingIssueEdition.openIssue()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "You are playing with the fire, kid. Sincerely yours, Chuck",
      "offset": 7
    }
  ],
  "location": "ExistingIssueEdition.updateIssueDescription(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "ExistingIssueEdition.updateIssue()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "ExistingIssueEdition.checkChangesApplied()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CommonUtil.signOut()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "id": "existing-issue-edition;editing-existing-bug;;3",
  "tags": [
    {
      "name": "@edit",
      "line": 1
    }
  ],
  "description": "",
  "name": "Editing existing bug",
  "keyword": "Scenario Outline",
  "line": 22,
  "type": "scenario"
});
formatter.step({
  "name": "RunMode is \"Y\"",
  "keyword": "Given ",
  "line": 11,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "I am on the home page in \"Chrome\"",
  "keyword": "And ",
  "line": 12,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "I open one of the issues",
  "keyword": "And ",
  "line": 13
});
formatter.step({
  "name": "I add \"You are playing with the fire, kid. Sincerely yours, Chuck\" to the issue description",
  "keyword": "And ",
  "line": 14,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "I update the issue",
  "keyword": "When ",
  "line": 15
});
formatter.step({
  "name": "My changes should be applied",
  "keyword": "Then ",
  "line": 16
});
formatter.step({
  "name": "I sign out",
  "keyword": "And ",
  "line": 17
});
formatter.match({
  "arguments": [
    {
      "val": "Y",
      "offset": 12
    }
  ],
  "location": "CommonUtil.checkRunMode(String)"
});
formatter.result({
  "duration": 85000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrome",
      "offset": 26
    }
  ],
  "location": "CommonUtil.loginWithDefaultCredentials(String)"
});
formatter.result({
  "duration": 5373838000,
  "status": "passed"
});
formatter.match({
  "location": "ExistingIssueEdition.openIssue()"
});
formatter.result({
  "duration": 4393709000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "You are playing with the fire, kid. Sincerely yours, Chuck",
      "offset": 7
    }
  ],
  "location": "ExistingIssueEdition.updateIssueDescription(String)"
});
formatter.result({
  "duration": 3321793000,
  "status": "passed"
});
formatter.match({
  "location": "ExistingIssueEdition.updateIssue()"
});
formatter.result({
  "duration": 154467000,
  "status": "passed"
});
formatter.match({
  "location": "ExistingIssueEdition.checkChangesApplied()"
});
formatter.result({
  "duration": 3240353000,
  "status": "passed"
});
formatter.match({
  "location": "CommonUtil.signOut()"
});
formatter.result({
  "duration": 1385530000,
  "status": "passed"
});
formatter.scenario({
  "id": "existing-issue-edition;editing-existing-bug;;4",
  "tags": [
    {
      "name": "@edit",
      "line": 1
    }
  ],
  "description": "",
  "name": "Editing existing bug",
  "keyword": "Scenario Outline",
  "line": 23,
  "type": "scenario"
});
formatter.step({
  "name": "RunMode is \"N\"",
  "keyword": "Given ",
  "line": 11,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "I am on the home page in \"IE\"",
  "keyword": "And ",
  "line": 12,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "I open one of the issues",
  "keyword": "And ",
  "line": 13
});
formatter.step({
  "name": "I add \"You are playing with the fire, kid. Sincerely yours, Chuck\" to the issue description",
  "keyword": "And ",
  "line": 14,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "I update the issue",
  "keyword": "When ",
  "line": 15
});
formatter.step({
  "name": "My changes should be applied",
  "keyword": "Then ",
  "line": 16
});
formatter.step({
  "name": "I sign out",
  "keyword": "And ",
  "line": 17
});
formatter.match({
  "arguments": [
    {
      "val": "N",
      "offset": 12
    }
  ],
  "location": "CommonUtil.checkRunMode(String)"
});
formatter.result({
  "duration": 383000,
  "status": "pending",
  "error_message": "cucumber.api.PendingException: Skipping the test dataset cause runmode \u003d N\n\tat com.jira.testcases.CommonUtil.checkRunMode(CommonUtil.java:24)\n\tat ✽.Given RunMode is \"N\"(main/resources/com/jira/testcases/issueManaging/ExistingIssueEdition.feature:11)\n"
});
formatter.match({
  "arguments": [
    {
      "val": "IE",
      "offset": 26
    }
  ],
  "location": "CommonUtil.loginWithDefaultCredentials(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "ExistingIssueEdition.openIssue()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "You are playing with the fire, kid. Sincerely yours, Chuck",
      "offset": 7
    }
  ],
  "location": "ExistingIssueEdition.updateIssueDescription(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "ExistingIssueEdition.updateIssue()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "ExistingIssueEdition.checkChangesApplied()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CommonUtil.signOut()"
});
formatter.result({
  "status": "skipped"
});
formatter.uri("main/resources/com/jira/testcases/issueManaging/SearchingForExistingIsuues.feature");
formatter.feature({
  "id": "searching-for-existing-issues",
  "tags": [
    {
      "name": "@search",
      "line": 1
    }
  ],
  "description": "\nAs a Jira user\nIn order to check the list of existing issues\nI want to be able to find them using Jira\u0027s search",
  "name": "Searching for existing issues",
  "keyword": "Feature",
  "line": 2
});
formatter.scenarioOutline({
  "id": "searching-for-existing-issues;searching-for-existing-issue",
  "description": "",
  "name": "Searching for existing issue",
  "keyword": "Scenario Outline",
  "line": 9,
  "type": "scenario_outline"
});
formatter.step({
  "name": "RunMode is \"\u003cRunMode\u003e\"",
  "keyword": "Given ",
  "line": 11
});
formatter.step({
  "name": "I am on the home page in \"\u003cbrowserType\u003e\"",
  "keyword": "And ",
  "line": 12
});
formatter.step({
  "name": "I perform a quick search for issue which contains \"\u003ctext\u003e\"",
  "keyword": "When ",
  "line": 13
});
formatter.step({
  "name": "At least one corresponding issue has to be found",
  "keyword": "Then ",
  "line": 14
});
formatter.step({
  "name": "I check that description of the top issue contains \"\u003ctext\u003e\"",
  "keyword": "And ",
  "line": 15
});
formatter.step({
  "name": "I sign out",
  "keyword": "And ",
  "line": 16
});
formatter.examples({
  "id": "searching-for-existing-issues;searching-for-existing-issue;",
  "description": "",
  "name": "",
  "keyword": "Examples",
  "line": 18,
  "rows": [
    {
      "id": "searching-for-existing-issues;searching-for-existing-issue;;1",
      "cells": [
        "RunMode",
        "browserType",
        "text"
      ],
      "line": 19
    },
    {
      "id": "searching-for-existing-issues;searching-for-existing-issue;;2",
      "cells": [
        "N",
        "Mozilla",
        "GrandMom"
      ],
      "line": 20
    },
    {
      "id": "searching-for-existing-issues;searching-for-existing-issue;;3",
      "cells": [
        "Y",
        "Chrome",
        "Jackie"
      ],
      "line": 21
    },
    {
      "id": "searching-for-existing-issues;searching-for-existing-issue;;4",
      "cells": [
        "N",
        "IE",
        "GrandMom"
      ],
      "line": 22
    }
  ]
});
formatter.scenario({
  "id": "searching-for-existing-issues;searching-for-existing-issue;;2",
  "tags": [
    {
      "name": "@search",
      "line": 1
    }
  ],
  "description": "",
  "name": "Searching for existing issue",
  "keyword": "Scenario Outline",
  "line": 20,
  "type": "scenario"
});
formatter.step({
  "name": "RunMode is \"N\"",
  "keyword": "Given ",
  "line": 11,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "I am on the home page in \"Mozilla\"",
  "keyword": "And ",
  "line": 12,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "I perform a quick search for issue which contains \"GrandMom\"",
  "keyword": "When ",
  "line": 13,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "At least one corresponding issue has to be found",
  "keyword": "Then ",
  "line": 14
});
formatter.step({
  "name": "I check that description of the top issue contains \"GrandMom\"",
  "keyword": "And ",
  "line": 15,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "I sign out",
  "keyword": "And ",
  "line": 16
});
formatter.match({
  "arguments": [
    {
      "val": "N",
      "offset": 12
    }
  ],
  "location": "CommonUtil.checkRunMode(String)"
});
formatter.result({
  "duration": 181000,
  "status": "pending",
  "error_message": "cucumber.api.PendingException: Skipping the test dataset cause runmode \u003d N\n\tat com.jira.testcases.CommonUtil.checkRunMode(CommonUtil.java:24)\n\tat ✽.Given RunMode is \"N\"(main/resources/com/jira/testcases/issueManaging/SearchingForExistingIsuues.feature:11)\n"
});
formatter.match({
  "arguments": [
    {
      "val": "Mozilla",
      "offset": 26
    }
  ],
  "location": "CommonUtil.loginWithDefaultCredentials(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "GrandMom",
      "offset": 51
    }
  ],
  "location": "SearchingForExistingIssues.doQuickSearch(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "SearchingForExistingIssues.findRelatedIssues()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "GrandMom",
      "offset": 52
    }
  ],
  "location": "SearchingForExistingIssues.checkDescriptionContains(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CommonUtil.signOut()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "id": "searching-for-existing-issues;searching-for-existing-issue;;3",
  "tags": [
    {
      "name": "@search",
      "line": 1
    }
  ],
  "description": "",
  "name": "Searching for existing issue",
  "keyword": "Scenario Outline",
  "line": 21,
  "type": "scenario"
});
formatter.step({
  "name": "RunMode is \"Y\"",
  "keyword": "Given ",
  "line": 11,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "I am on the home page in \"Chrome\"",
  "keyword": "And ",
  "line": 12,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "I perform a quick search for issue which contains \"Jackie\"",
  "keyword": "When ",
  "line": 13,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "At least one corresponding issue has to be found",
  "keyword": "Then ",
  "line": 14
});
formatter.step({
  "name": "I check that description of the top issue contains \"Jackie\"",
  "keyword": "And ",
  "line": 15,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "I sign out",
  "keyword": "And ",
  "line": 16
});
formatter.match({
  "arguments": [
    {
      "val": "Y",
      "offset": 12
    }
  ],
  "location": "CommonUtil.checkRunMode(String)"
});
formatter.result({
  "duration": 175000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Chrome",
      "offset": 26
    }
  ],
  "location": "CommonUtil.loginWithDefaultCredentials(String)"
});
formatter.result({
  "duration": 5122578000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jackie",
      "offset": 51
    }
  ],
  "location": "SearchingForExistingIssues.doQuickSearch(String)"
});
formatter.result({
  "duration": 3252371000,
  "status": "passed"
});
formatter.match({
  "location": "SearchingForExistingIssues.findRelatedIssues()"
});
formatter.result({
  "duration": 3116070000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jackie",
      "offset": 52
    }
  ],
  "location": "SearchingForExistingIssues.checkDescriptionContains(String)"
});
formatter.result({
  "duration": 676699000,
  "status": "passed"
});
formatter.match({
  "location": "CommonUtil.signOut()"
});
formatter.result({
  "duration": 1744932000,
  "status": "passed"
});
formatter.scenario({
  "id": "searching-for-existing-issues;searching-for-existing-issue;;4",
  "tags": [
    {
      "name": "@search",
      "line": 1
    }
  ],
  "description": "",
  "name": "Searching for existing issue",
  "keyword": "Scenario Outline",
  "line": 22,
  "type": "scenario"
});
formatter.step({
  "name": "RunMode is \"N\"",
  "keyword": "Given ",
  "line": 11,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "I am on the home page in \"IE\"",
  "keyword": "And ",
  "line": 12,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "I perform a quick search for issue which contains \"GrandMom\"",
  "keyword": "When ",
  "line": 13,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "At least one corresponding issue has to be found",
  "keyword": "Then ",
  "line": 14
});
formatter.step({
  "name": "I check that description of the top issue contains \"GrandMom\"",
  "keyword": "And ",
  "line": 15,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "I sign out",
  "keyword": "And ",
  "line": 16
});
formatter.match({
  "arguments": [
    {
      "val": "N",
      "offset": 12
    }
  ],
  "location": "CommonUtil.checkRunMode(String)"
});
formatter.result({
  "duration": 172000,
  "status": "pending",
  "error_message": "cucumber.api.PendingException: Skipping the test dataset cause runmode \u003d N\n\tat com.jira.testcases.CommonUtil.checkRunMode(CommonUtil.java:24)\n\tat ✽.Given RunMode is \"N\"(main/resources/com/jira/testcases/issueManaging/SearchingForExistingIsuues.feature:11)\n"
});
formatter.match({
  "arguments": [
    {
      "val": "IE",
      "offset": 26
    }
  ],
  "location": "CommonUtil.loginWithDefaultCredentials(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "GrandMom",
      "offset": 51
    }
  ],
  "location": "SearchingForExistingIssues.doQuickSearch(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "SearchingForExistingIssues.findRelatedIssues()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "GrandMom",
      "offset": 52
    }
  ],
  "location": "SearchingForExistingIssues.checkDescriptionContains(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CommonUtil.signOut()"
});
formatter.result({
  "status": "skipped"
});
});