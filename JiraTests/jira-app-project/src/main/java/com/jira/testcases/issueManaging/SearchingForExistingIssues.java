package com.jira.testcases.issueManaging;

import org.junit.Assert;
import org.openqa.selenium.Keys;

import com.jira.pages.DashboardPage;
import com.jira.pages.ExistingIssuePage;
import com.jira.pages.IssuesFilterPage;
import com.jira.pages.NewIssuePage;
import com.jira.pages.SignInPage;
import com.jira.pages.SignOutPage;

import com.jira.util.WebConnector;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/*SearchingForExistingIssues class contains methods which implement keywords of the appropriate feature*/

public class SearchingForExistingIssues {
	
	SignInPage loginPage;
	DashboardPage dashboardPage;
	SignOutPage signOutPage;
	NewIssuePage newIssuePage;
	ExistingIssuePage existingIssuePage;
	IssuesFilterPage issuesFilterPage;
	
	WebConnector Selenium = WebConnector.getInstance();
	
	
	//When I perform a quick search for issue which contains "<text>"
	@When ("^I perform a quick search for issue which contains \"([^\"]*)\"$") 
	public void doQuickSearch (String text)  {
		dashboardPage = DashboardPage.getInstance(Selenium.getDriver());
			String result = Selenium.typeWebElement(text, dashboardPage.getQuickSearchField());	
			Assert.assertEquals(result, Selenium.PASS);				
			result = Selenium.hitEnter(dashboardPage.getQuickSearchField());	
			Assert.assertEquals(result, Selenium.PASS);
		}
		
	//Then At least one corresponding issue has to be found
	@Then ("^At least one corresponding issue has to be found$") 
	public void findRelatedIssues ()  {
		issuesFilterPage = IssuesFilterPage.getInstance(Selenium.getDriver());
		String result = Selenium.refreshPage();	
		Assert.assertEquals(result, Selenium.PASS);
		result = Selenium.clickWebElement(issuesFilterPage.getFirstFoundIssueLink());
		Assert.assertEquals(result, Selenium.PASS);				
		}
	
	//And I check that description of the top issue contains "<text>"
	@And ("^I check that description of the top issue contains \"([^\"]*)\"$") 
	public void checkDescriptionContains (String expectedText)  {	
		existingIssuePage = ExistingIssuePage.getInstance(Selenium.getDriver());
		String result=Selenium.checkTextInsideElement(existingIssuePage.getDescriptionFieldWithPencil(), expectedText);
		Assert.assertEquals(result, Selenium.PASS);
		}

}
